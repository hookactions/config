module gitlab.com/hookactions/config

go 1.12

require (
	github.com/aws/aws-sdk-go-v2 v0.9.0
	github.com/kr/pty v1.1.8 // indirect
	github.com/pkg/errors v0.8.0
	github.com/stretchr/testify v1.2.2
	golang.org/x/crypto v0.0.0-20190820162420-60c769a6c586 // indirect
	golang.org/x/lint v0.0.0-20190409202823-959b441ac422 // indirect
	golang.org/x/mod v0.1.0 // indirect
	golang.org/x/net v0.0.0-20190813141303-74dc4d7220e7 // indirect
	golang.org/x/sys v0.0.0-20190813064441-fde4db37ae7a // indirect
	golang.org/x/text v0.3.2 // indirect
	golang.org/x/tools v0.0.0-20190821162956-65e3620a7ae7 // indirect
	honnef.co/go/tools v0.0.1-2019.2.2 // indirect
)
